package es.slothdevelopers.slothfraework.customheaderlistview.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import es.slothdevelopers.slothfraework.customheaderlistview.R;
import es.slothdevelopers.slothfraework.customheaderlistview.model.Model;
import es.slothdevelopers.slothframework.adapters.SlothArrayAdapter;
import es.slothdevelopers.slothframework.annotations.InjectView;

public class ModelAdapter extends SlothArrayAdapter<Model> {

    @InjectView(R.id.name)          TextView name;
    @InjectView(R.id.description)   TextView description;

    public ModelAdapter(Context mContext, int layoutResourceId, List<Model> data) {
        super(mContext, layoutResourceId, data);
    }

    @Override
    protected void onCreateViewForPosition(View viewCreated, int position, Model data) {
        name.setText(data.getName());
        description.setText(data.getDescription());
    }
}
