package es.slothdevelopers.slothfraework.customheaderlistview;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

import es.slothdevelopers.slothfraework.customheaderlistview.adapter.ModelAdapter;
import es.slothdevelopers.slothfraework.customheaderlistview.model.Model;
import es.slothdevelopers.slothframework.activity.SlothActionBarActivity;
import es.slothdevelopers.slothframework.annotations.InjectView;


public class MainActivity extends SlothActionBarActivity implements AbsListView.OnScrollListener {

    private static final int NUMBER_OF_ELEMENTS = 40;

    private int lastTopValueAssigned = 0;

    ArrayAdapter<Model> adapter;
    List<Model> modelList = new LinkedList<Model>();

    @InjectView(android.R.id.list)
    ListView listView;

    // we can not inject views that belong to the header because is inflacted AFTER the injection
    // (the injection is done in setContentView)
    ImageView backgroundImage;
    Button postButton;

    public MainActivity() {
        initListElements();
    }

    private void initListElements() {
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            modelList.add(new Model("Name " + i, "Content description " + i));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inflateHeader();
        initAdapter();
        addScrollListenerForSDKsAbove11();

        // just for aestetics
        postButton.setSelected(true);
    }



    private void initAdapter() {
        // instantiate the adapter and attach it to the listview
        adapter = new ModelAdapter(this, R.layout.element_list_view, modelList);
        listView.setAdapter(adapter);
    }

    private void inflateHeader() {
        // inflate custom header and attach it to the list
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.custom_header, listView, false);
        listView.addHeaderView(header, null, false);

        // we take the background image and button reference from the header
        backgroundImage = (ImageView) header.findViewById(R.id.customHeaderBackground);
        postButton = (Button) header.findViewById(R.id.postsButton);
    }

    private void addScrollListenerForSDKsAbove11() {
        if (Integer.valueOf(Build.VERSION.SDK_INT)>11) {
            // setting onScroll listener
            listView.setOnScrollListener(this);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        parallaxImage(backgroundImage);
    }

    private void parallaxImage(View view) {
        Rect rect = new Rect();
        view.getLocalVisibleRect(rect);
        if (lastTopValueAssigned != rect.top) {
            lastTopValueAssigned = rect.top;
            view.setY((float) (rect.top / 2.0));
        }
    }
}
